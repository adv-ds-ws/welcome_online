# Welcome Online

Hello and welcome to the online version of the Advisori Python workshop.

In this repository you will find information about how to access the online resources of the workshop (PDF guide - How to access the Workshop Cloud Resources). In addition there are a Jupyter Notebook and a few code files (exercise.py, etc. ). Open the notebook via the Jupyter Server and check if your environment works as expected. In addition you can use the notebook as a playground to familiarize yourself with the environment. Open the exercise.py from within VS Code. It contains a functioning implementation of the Hangman game `[Wikipedia](https://en.wikipedia.org/wiki/Hangman_(game))`. Use the code to get acquainted with Python and try to extend it as proposed in the header of the exercise.py file.

