"""
Hi, and welcome to the online version of our Python workshop. As a teaser and a 
little refresher we have prepared an exercise for you. Below you see an implementation
of the game of hangman. You can start the game by running the code below and following the
instructions (enter commands in the commandline below/terminal). 

The game is almost functional but one thing is missing: You are prompted to set a difficulty
level between 0 and 4 - but this part hasn't been implemented yet.

Go ahead to the get_word function and think of a way to implement a difficulty level.Tip: Filter 
the list of words WORD_LIST in a sensible way e.g. limiting the length of words for each difficulty level.

Bonus1: As a refresher annotate the code below with meaningful comments (if necessary). 
Bonus2: Does the code follow the clean-code ideals? What about the Python Zen below:

The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!

Bonus 3: Implement support for more languages.

"""

import random
#TODO: Bonus: Can you implement a language selector and add another language?
from word_data import WORD_LIST
from picture_data import HANGMAN_PICTURES


def get_word(difficulty = 3):
    trimmed_words = WORD_LIST
    #TODO: Implement difficulty levels from 0 to 4. The more difficult the level the longer the word can be.

    word = random.choice(trimmed_words).lower()
    return word


def get_masked_word(word, guessed_letters):
    masked_letters  = [letter for letter in set(list(word)) if letter not in guessed_letters]
    masked_word = word
    for masked_letter in masked_letters:
        masked_word = masked_word.replace(masked_letter, "_")
    return masked_word

def main_loop():
    print("Setting up a new game of hangman...")
    #Initialize
    while(True):
        difficulty = int(input("Please select a difficulty from 0 to 4."))
        if difficulty not in [0,1,2,3,4]:
            print("Please try again.")
        break


    word = get_word(difficulty = difficulty)
    max_hits = len(HANGMAN_PICTURES)
    hits = 0
    guessed_letters = []

    print("Ok, I got my word. Let's get started.")
    #begin game
    while(hits<max_hits-1):
        masked_word = get_masked_word(word, guessed_letters)
        if masked_word == word:
            print("Congratulations - you won!")
            print(masked_word)
            return

        print(HANGMAN_PICTURES[hits])
        print(f"Guessed letters: {guessed_letters}")
        print(" ".join(masked_word))

        while(True):
            input_letter = str(input("Please enter a letter: ")).lower()
            if len(input_letter) > 1:
                print("Please only enter a single letter!")
            elif len(input_letter) == 0:
                print("Please enter something!")
            elif input_letter in guessed_letters:
                print("You already guessed this letter. Use another one!")
            else:
                guessed_letters.append(input_letter)
                if input_letter not in word:
                    hits += 1
                break
    print(f"Sorry, you didn't make it. The word I have been looking for was - {word} - and you got to - {masked_word} -.")    

if __name__ == "__main__":
    answer = input("Hello there, are you in for a game of hangman? (y/n)")
    if answer == "y":
        while(True):
            main_loop()
            answer = input("Play again? (y/n)")
            if answer == "n":
                break
    print("Thanks for playing. Bye, then!")