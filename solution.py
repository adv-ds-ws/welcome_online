#ToDo: Formulate an exercise.
import random
from word_data import WORD_LIST
from picture_data import HANGMAN_PICTURES


def get_word(difficulty = 3):
    difficulty_mapping = [(1,3),
                        (4,6),
                        (7,9),
                        (10,12),
                        (13,0)]
    min_length, max_length = difficulty_mapping[difficulty]

    trimmed_words = [word for word in WORD_LIST if len(word) >= min_length]
    if max_length:
        trimmed_words = [word for word in trimmed_words if len(word) <= max_length]

    word = random.choice(trimmed_words).lower()
    return word


def get_masked_word(word, guessed_letters):
    masked_letters  = [letter for letter in set(list(word)) if letter not in guessed_letters]
    masked_word = word
    for masked_letter in masked_letters:
        masked_word = masked_word.replace(masked_letter, "_")
    return masked_word

def main_loop():
    print("Setting up a new game of hangman...")
    #Initialize
    while(True):
        difficulty = int(input("Please select a difficulty from 0 to 4."))
        if difficulty not in [0,1,2,3,4]:
            print("Please try again.")
        break


    word = get_word(difficulty = difficulty)
    max_hits = len(HANGMAN_PICTURES)
    hits = 0
    guessed_letters = []

    print("Ok, I got my word. Let's get started.")
    #begin game
    while(hits<max_hits-1):
        masked_word = get_masked_word(word, guessed_letters)
        if masked_word == word:
            print("Congratulations - you won!")
            print(masked_word)
            return

        print(HANGMAN_PICTURES[hits])
        print(f"Guessed letters: {guessed_letters}")
        print(" ".join(masked_word))

        while(True):
            input_letter = str(input("Please enter a letter: ")).lower()
            if len(input_letter) > 1:
                print("Please only enter a single letter!")
            elif len(input_letter) == 0:
                print("Please enter something!")
            elif input_letter in guessed_letters:
                print("You already guessed this letter. Use another one!")
            else:
                guessed_letters.append(input_letter)
                if input_letter not in word:
                    hits += 1
                break
    print(f"Sorry, you didn't make it. The word I have been looking for was - {word} - and you got to - {masked_word} -.")    

if __name__ == "__main__":
    answer = input("Hello there, are you in for a game of hangman? (y/n)")
    if answer == "y":
        while(True):
            main_loop()
            answer = input("Play again? (y/n)")
            if answer == "n":
                break
    print("Thanks for playing. Bye, then!")